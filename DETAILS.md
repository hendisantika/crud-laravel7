# Laravel CRUD APP

### Things to do list:
1. Clone this repository: `https://gitlab.com/hendisantika/crud-laravel7.git`
2. Go inside the folder: `cd crud-laravel7`
3. Run `cp .env.example .env` & set your desired db name
4. Run `composer install` 
5. Run `php artisan migrate:refresh --seed`
6. Run `php artisan serve`
7. Open your favorite browser: http://localhost:8000/pegawai

### Screen shot
Add New Pegawai

![Add New Pegawai](img/add.png "Add New Pegawai")

List All Pegawai

![List All Pegawai](img/list.png "List All Pegawai")

Update Pegawai

![Update Pegawai](img/update.png "Update Pegawai") 
