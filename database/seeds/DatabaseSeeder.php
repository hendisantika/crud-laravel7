<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $path = './database/seeds/sql/pegawai.sql';

        DB::unprepared(File::get(base_path($path)));
    }
}
